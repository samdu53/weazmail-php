<?php

namespace SamB\WeazMail;

/**
 * WeazMail wrapper PHP
 * 
 * @author Samuel Besnier <sbesnier1901@gmail.com>
 */
class WeazMail 
{
    private $api_key;
    private $base_url            = 'https://api.weazmail.com/v1';
    private $defaultReplyAddress = null;
    private $defaultFrom         = null;
    private $defaultAlias        = null;

    const TIMEOUT = 10;

    /**
     * Create a new instance
     *
     * @throws \Exception
     */
    public function __construct($config)
    {
        if (!function_exists('curl_init') || !function_exists('curl_setopt')) {
            throw new \Exception("cURL support is required, but can't be found.");
        }

        if (is_object($config)) {
            if (property_exists($config, "defaultReplyAddress")) {
                $this->defaultReplyAddress = $config->defaultReplyAddress;
            }
            if (property_exists($config, "defaultFrom")) {
                $this->defaultFrom = $config->defaultFrom;
            }
            if (property_exists($config, "defaultAlias")) {
                $this->defaultAlias = $config->defaultAlias;
            }
            $this->api_key = $config->api_key;
        } else {
            $this->api_key = $config;
        }

        if (preg_match('/^[a-zA-Z0-9\-_]+?\.[a-zA-Z0-9\-_]+?\.([a-zA-Z0-9\-_]+)?$/', $this->api_key) === false) {
            throw new \Exception("Invalid WeazMail API key supplied.");
        }
    }

    public function SendEmail($options)
    {
        if (!is_object( $options )) {
           throw new \Exception('SendEmail is waiting for an object. ' . gettype($options) . ' given');
        }

        $options->path   = 'transactional/send';
        $options->method = 'post';

        return $this->request($options);
    }

    /**
     * Make a request
     */
    private function request($options, $timeout = self::TIMEOUT)
    {
        $url = $this->base_url . '/' . $options->path;

        $httpHeader = array(
            'Accept: application/json',
            'Content-Type: application/json',
            'api_key: ' . $this->api_key
        );
        if ($options->method === 'put') {
            $httpHeader[] = 'Allow: PUT, PATCH, POST';
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'SamB/WeazMail-API/v1');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);

        $postDatas = json_encode($options->params);

        switch ($options->method) {
            case 'post':
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postDatas);
                $httpHeader[] = 'Content-Length: ' . strlen($postDatas);
                break;
            case 'get':
                $query = http_build_query($args, '', '&');
                curl_setopt($ch, CURLOPT_URL, $url . '?' . $query);
                break;
            case 'delete':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;
            case 'patch':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postDatas);
                $httpHeader[] = 'Content-Length: ' . strlen($postDatas);
                break;
            case 'put':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postDatas);
                $httpHeader[] = 'Content-Length: ' . strlen($postDatas);
                break;
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeader);
        
        $response    = curl_exec($ch);

        $statusCode  = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);

        $header      = substr($response, 0, $header_size);

        $body        = substr($response, $header_size);

        if (!empty($body)) {
            $body = json_decode($body);
        }

        switch ($statusCode) {
            case '403':
                throw new \Exception('Bad credentials');
                break;
            
            case '501':
                throw new \Exception($body->message);
                break;
        }

        curl_close($ch);
        
        return $body;
    }
}
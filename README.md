# WeazMail wrapper pour PHP

## Installation :
```
composer require sambesnier/weazmail-php
```

## Utilisation :
```php
require_once __DIR__ . '/vendor/autoload.php';

use SamB\WeazMail\WeazMail;

$weazmail = new WeazMail('API_KEY');

$options = new stdClass();

$options->params = [
    'template'     => '<template>',
    'lang'         => '<code>',
    'subject'      => '<subject>',
    'to'           => '<to>',
    'from'         => '<from>',         // Optional
    'alias'        => '<alias>',        // Optional
    'replyAddress' => '<replyAddress>', // Optional
    'variables'    => [                 // Optional
        ['[#VAR#]' => '<var>'],
        ...
    ]
];

try {
    $response = $weazmail->SendEmail(
        $options
    );

    var_dump($response->message);
} catch (Exception $error) {
    var_dump($error->getMessage());
}
```

